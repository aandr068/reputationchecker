package com.gigamog.controller;

import com.gigamog.model.UnauthorizedPayload;
import com.gigamog.model.User;
import com.gigamog.model.reddit.RedditOauth;
import com.gigamog.model.reddit.redditMe.RedditMe;
import com.gigamog.service.RedditMeService;
import com.gigamog.service.RedditOauthService;
import com.gigamog.service.UserService;
import com.gigamog.service.SessionService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@AllArgsConstructor
@RestController
public class AuthController {

    UserService reputationCheckerUserService;
    private static final String REDIRECT_TO_BASE_URL = "<script type=\"application/javascript\">window.location.href = window.location.origin;</script>";
    private final RedditOauthService redditOauthService;
    private final UserService userService;
    private final RedditMeService redditMeService;
    private final SessionService sessionService;

    @RequestMapping(method = RequestMethod.GET, path = "/auth")
    public ResponseEntity getUser(){
        if(sessionService.isAuthenticate()){
            return ResponseEntity.ok(sessionService.getUser());
        }
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new UnauthorizedPayload(redditOauthService.generateAuthUrl()));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/redirect")
    public ResponseEntity  redirectWithUsingRedirectView(
            @RequestParam("code") String code, @RequestParam("state") String state) {
        Optional<RedditOauth> redditOauthOptional = redditOauthService.getAccessToken(code, state);
        if(redditOauthOptional.isPresent()){
            Optional<RedditMe> redditMeOptional = redditMeService.getRedditMe(redditOauthOptional.get().getAccessToken());
            if(redditMeOptional.isPresent()){
                createUser(redditOauthOptional.get(), redditMeOptional.get());
                return ResponseEntity.ok(REDIRECT_TO_BASE_URL);
            }
        }
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .build();
    }

    private void createUser(final RedditOauth redditOauth, RedditMe redditMe){
        User user = new User(redditMe.getName(), redditOauth.getAccessToken(), redditMe.getId());
        userService.upsert(user);
        sessionService.setUser(user);
    }

}
