package com.gigamog.config;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsConfig {

    @Bean
    public AmazonDynamoDB getAmazonClient(ProfileCredentialsProvider profileCredentialsProvider){
        return AmazonDynamoDBClientBuilder.standard()
                .withCredentials(profileCredentialsProvider)
                .withRegion(Regions.US_EAST_1)
                .build();
    }

    @Bean
    public ProfileCredentialsProvider getCredentialProvider(){
        return new ProfileCredentialsProvider();
    }

    @Bean
    public DynamoDBMapper getDynamoDbMapper(AmazonDynamoDB amazonDynamoDB){
        return new DynamoDBMapper(amazonDynamoDB,
                new DynamoDBMapperConfig.Builder()
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE)
                        .build());
    }
}
