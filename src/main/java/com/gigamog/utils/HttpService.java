package com.gigamog.utils;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigamog.model.reddit.redditMe.RedditMe;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.CharEncoding;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import javax.net.ssl.HttpsURLConnection;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Slf4j
@AllArgsConstructor
@Service
public class HttpService {
    private static final String POST = "POST";
    private static final String GET = "GET";
    private static final String USER_AGENT = "User-Agent";
    private static final String CLIENT = "Java Client";
    private static final String AUTHORIZATION = "Authorization";
    private static final String CONTENT_TYPE = "Content-Type";

    private final ObjectMapper objectMapper;


    public <T> CompletableFuture<T> doPostAsync(String url, String auth, Object postBody, Class<T> tClass) {
        CompletableFuture<T> result = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
                    try {
                        return result.complete(internalDoPost(url, auth, postBody, tClass));
                    } catch (IOException e) {
                        log.info("failed post", e);
                        return result.completeExceptionally(e);
                    }
                }
        );
        return result;
    }


    public <T> Optional<T> doPost(String url, String auth, Object postBody, Class<T> tClass) {
        try {
            return Optional.of(internalDoPost(url, auth, postBody, tClass));
        } catch (Exception e) {
            log.info("failed post", e);
            return Optional.empty();
        }
    }

    public <T> CompletableFuture<T> doGetAsync(String url, String auth, Class<T> tClass) {
        CompletableFuture<T> result = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
                    try {
                        return result.complete(internalDoGet(url, auth, tClass));
                    } catch (IOException e) {
                        log.info("failed post", e);
                        return result.completeExceptionally(e);
                    }
                }
        );
        return result;
    }

    public <T> Optional<T> doGet(String url, String auth, Class<T> tClass) {
        try {
            return Optional.of(internalDoGet(url, auth, tClass));
        } catch (Exception e) {
            log.info("failed get", e);
            return Optional.empty();
        }
    }


    private <T> T internalDoGet(String url, String auth, Class<T> tClass) throws IOException{
        URL urlObj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
        con.setRequestMethod(GET);
        setGenerics(con, auth);
        return objectMapper.readValue(con.getInputStream(), tClass);
    }

    private <T> T internalDoPost(String url, String auth, Object postBody, Class<T> tClass) throws IOException {
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod(POST);
        setGenerics(con, auth);
        String accessTokenRequest = getUrlEncodedForm(postBody);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(accessTokenRequest);
        wr.flush();
        wr.close();
        return objectMapper.readValue(con.getInputStream(), tClass);
    }

    private void setGenerics(URLConnection con, String auth) {
        con.setRequestProperty(USER_AGENT, CLIENT);
        con.setRequestProperty(CONTENT_TYPE, ContentType.APPLICATION_FORM_URLENCODED.getMimeType());
        con.setRequestProperty(AUTHORIZATION, auth);
    }

    private String getUrlEncodedForm(Object obj) {
        JsonNode rootNode = objectMapper.valueToTree(obj);
        List<NameValuePair> form = new ArrayList<>();
        Iterator<Map.Entry<String, JsonNode>> fields = rootNode.fields();
        while (fields.hasNext()) {
            Map.Entry<String, JsonNode> entry = fields.next();
            form.add(new BasicNameValuePair(entry.getKey(), entry.getValue().textValue()));
        }
        return URLEncodedUtils.format(form, CharEncoding.UTF_8);
    }
}
