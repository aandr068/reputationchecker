package com.gigamog.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
@Getter
@AllArgsConstructor
@DynamoDBTable(tableName="ReputationCheckerUser")
public class User {
    @DynamoDBHashKey
    private String userName;
    private String accessToken;
    private String id;
}
