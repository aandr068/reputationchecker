
package com.gigamog.model.reddit.redditMe;

import lombok.Data;

@Data
public class RedditMe {

    private Boolean isEmployee;
    private Boolean hasVisitedNewProfile;
    private Boolean prefNoProfanity;
    private Boolean isSuspended;
    private String prefGeopopular;
    private Boolean prefShowTrending;
    private Boolean isSponsor;
    private String id;
    private Boolean verified;
    private String oauthClientId;
    private Features features;
    private Boolean over18;
    private Boolean isGold;
    private Boolean isMod;
    private Boolean hasVerifiedEmail;
    private Boolean inRedesignBeta;
    private String iconImg;
    private Integer draftsCount;
    private Boolean hideFromRobots;
    private Integer linkKarma;
    private Integer inboxCount;
    private Object prefTopKarmaSubreddits;
    private Boolean prefShowSnoovatar;
    private String name;
    private Integer prefClickgadget;
    private Float created;
    private Integer goldCreddits;
    private Float createdUtc;
    private Boolean inBeta;
    private Integer commentKarma;
    private Boolean hasSubscribed;
}
