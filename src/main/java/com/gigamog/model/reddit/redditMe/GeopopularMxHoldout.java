
package com.gigamog.model.reddit.redditMe;

import lombok.Data;

@Data
public class GeopopularMxHoldout {

    private String owner;
    private String variant;
    private Integer experimentId;

}
