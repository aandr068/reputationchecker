
package com.gigamog.model.reddit.redditMe;

import lombok.Data;

@Data
public class LogisticRegressionV5 {

    private String owner;
    private String variant;
    private Integer experimentId;

}
