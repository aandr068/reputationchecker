package com.gigamog.model.reddit.redditMe;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.gigamog.model.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

@AllArgsConstructor
@Repository
public class ReputationCheckerUserRepository {

    DynamoDBMapper dynamoDBMapper;

    public User find(User user){
        return dynamoDBMapper.load(User.class, user);
    }

    public void save(User user){
        dynamoDBMapper.save(user);
    }

}
