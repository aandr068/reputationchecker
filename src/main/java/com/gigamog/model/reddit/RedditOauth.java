
package com.gigamog.model.reddit;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RedditOauth {
    private String accessToken;
    private String tokenType;
    private Integer expiresIn;
    private String refreshToken;
    private String scope;
}
