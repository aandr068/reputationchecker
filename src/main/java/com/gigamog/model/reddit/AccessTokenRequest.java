package com.gigamog.model.reddit;

import lombok.Getter;

@Getter
public class AccessTokenRequest {
    private String grantType = "authorization_code";
    private String code;
    private String redirectUri;

    public AccessTokenRequest(String code, String redirectUri) {
        this.code = code;
        this.redirectUri = redirectUri;
    }
}
