package com.gigamog.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigamog.utils.HttpService;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class RedditApi {
    protected static final String BEARER = "Bearer ";

    protected final HttpService httpService;
    protected final ObjectMapper objectMapper;
    protected final SessionService sessionService;


    protected String getAuth(){
        return BEARER.concat(sessionService.getUser().getAccessToken());
    }
}
