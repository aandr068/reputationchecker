package com.gigamog.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigamog.model.reddit.redditMe.RedditMe;
import com.gigamog.utils.HttpService;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
public class RedditMeService extends RedditApi {
    private static final String ME_URL = "https://oauth.reddit.com/api/v1/me";

    public RedditMeService(HttpService httpService, ObjectMapper objectMapper, SessionService sessionService) {
        super(httpService, objectMapper, sessionService);
    }

    public Optional<RedditMe> getRedditMe(String token)  {
        return httpService.doGet(ME_URL, getAuth(token), RedditMe.class);
    }


    public CompletableFuture<RedditMe> getRedditMeAsync(String token)  {
        return httpService.doGetAsync(ME_URL, getAuth(token), RedditMe.class);
    }

    public CompletableFuture<RedditMe> getRedditMe()  {
        return httpService.doGetAsync(ME_URL, getAuth(), RedditMe.class);
    }

    private String getAuth(String token){
        return BEARER.concat(token);
    }

}
