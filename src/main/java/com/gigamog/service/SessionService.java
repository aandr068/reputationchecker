package com.gigamog.service;

import com.gigamog.model.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@AllArgsConstructor
@Service
public class SessionService {
    private static final String STATE = "state";
    private static final String AUTH = "user";
    private final HttpSession session;

    public void setState(String token){
        session.setAttribute(STATE, token);
    }

    public String getState(){
        return session.getAttribute(STATE).toString();
    }

    public void setUser(User user){
        session.setAttribute(AUTH, user);
    }

    public User getUser(){
        return (User) session.getAttribute(AUTH);
    }

    public Boolean isAuthenticate(){
        return getUser() != null;
    }

}
