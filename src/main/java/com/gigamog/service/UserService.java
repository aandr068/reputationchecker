package com.gigamog.service;

import com.gigamog.model.User;
import com.gigamog.model.reddit.redditMe.ReputationCheckerUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@AllArgsConstructor
@Service
public class UserService {

    private ReputationCheckerUserRepository repository;

    public CompletableFuture<User> upsertAsync(final User user){
        repository.save(user);
        return CompletableFuture.completedFuture(user);
    }

    public void upsert(final User user){
        repository.save(user);
    }





}
