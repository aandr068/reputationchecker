package com.gigamog.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigamog.config.RedditConfiguration;
import com.gigamog.model.reddit.AccessTokenRequest;
import com.gigamog.model.reddit.RedditOauth;
import com.gigamog.utils.HttpService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;


import java.security.SecureRandom;
import java.util.Base64;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
@AllArgsConstructor
public class RedditOauthService {
    private static final String OAUTH_URL =
            "https://www.reddit.com/api/v1/authorize?client_id=%s&response_type=code&state=%s&redirect_uri=%s&duration=temporary&scope=identity,history";

    private static final String ACCESS_TOKEN_URL = "https://www.reddit.com/api/v1/access_token";

    private final ObjectMapper objectMapper;
    private final RedditConfiguration redditConfiguration;
    private final SessionService sessionService;
    private final HttpService httpService;

    public String generateAuthUrl() {
        String token = generateSafeToken();
        sessionService.setState(token);
        return String.format(OAUTH_URL, redditConfiguration.getClientID(), token, redditConfiguration.getRedirectUrl());
    }


    public CompletableFuture<RedditOauth> getAccessTokenFuture(String code, String state)  throws IllegalArgumentException{
        Assert.isTrue(sessionService.getState().matches(state), "invalid code");
        String basicAuth = getBasicAuth(redditConfiguration.getClientID(), redditConfiguration.getClientSecret());
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(code, redditConfiguration.getRedirectUrl());
        return httpService.doPostAsync(ACCESS_TOKEN_URL, basicAuth, accessTokenRequest, RedditOauth.class);
    }

    public Optional<RedditOauth> getAccessToken(String code, String state)  throws IllegalArgumentException{
        Assert.isTrue(sessionService.getState().matches(state), "invalid code");
        String basicAuth = getBasicAuth(redditConfiguration.getClientID(), redditConfiguration.getClientSecret());
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(code, redditConfiguration.getRedirectUrl());
        return httpService.doPost(ACCESS_TOKEN_URL, basicAuth, accessTokenRequest, RedditOauth.class);
    }

    private String getBasicAuth(String clientID, String clientSecret) {
        return "Basic ".concat(Base64.getEncoder()
                .encodeToString(clientID.concat(":").concat(clientSecret).getBytes()));
    }

    private String generateSafeToken() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[10];
        random.nextBytes(bytes);
        Base64.Encoder encoder = Base64.getUrlEncoder().withoutPadding();
        String token = encoder.encodeToString(bytes);
        return token;
    }



}
