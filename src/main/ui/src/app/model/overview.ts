 export interface SecureMediaEmbed {
  }

  export interface MediaEmbed {
  }

  export interface Data2 {
    link_url: string;
    subreddit_id: string;
    approved_at_utc?: any;
    edited: any;
    mod_reason_by?: any;
    banned_by?: any;
    removal_reason?: any;
    link_id: string;
    author_flair_template_id?: any;
    likes: boolean;
    replies: string;
    user_reports: any[];
    saved: boolean;
    id: string;
    banned_at_utc?: any;
    mod_reason_title?: any;
    gilded: number;
    archived: boolean;
    report_reasons?: any;
    author: string;
    num_comments: number;
    can_mod_post: boolean;
    send_replies: boolean;
    parent_id: string;
    score: number;
    approved_by?: any;
    over_18: boolean;
    mod_note?: any;
    collapsed: boolean;
    body: string;
    link_title: string;
    author_flair_css_class?: any;
    downs: number;
    is_submitter: boolean;
    collapsed_reason?: any;
    body_html: string;
    distinguished?: any;
    stickied: boolean;
    can_gild: boolean;
    subreddit: string;
    name: string;
    score_hidden: boolean;
    permalink: string;
    num_reports?: any;
    link_permalink: string;
    no_follow: boolean;
    link_author: string;
    author_flair_text?: any;
    quarantine: boolean;
    created: number;
    created_utc: number;
    subreddit_name_prefixed: string;
    controversiality: number;
    mod_reports: any[];
    subreddit_type: string;
    ups: number;
    is_crosspostable?: boolean;
    wls?: any;
    thumbnail_width?: any;
    selftext_html: string;
    selftext: string;
    suggested_sort?: any;
    secure_media?: any;
    is_reddit_media_domain?: boolean;
    view_count?: number;
    clicked?: boolean;
    num_crossposts?: number;
    link_flair_text?: any;
    pinned?: boolean;
    domain: string;
    hidden?: boolean;
    pwls?: any;
    thumbnail: string;
    link_flair_css_class?: any;
    contest_mode?: boolean;
    locked?: boolean;
    subreddit_subscribers?: number;
    secure_media_embed: SecureMediaEmbed;
    media_embed: MediaEmbed;
    visited?: boolean;
    thumbnail_height?: any;
    spoiler?: boolean;
    parent_whitelist_status?: any;
    hide_score?: boolean;
    url: string;
    title: string;
    media?: any;
    is_self?: boolean;
    whitelist_status?: any;
    is_video?: boolean;
  }

  export interface Child {
    kind: string;
    data: Data2;
  }

  export interface Data {
    modhash: string;
    dist: number;
    children: Child[];
    after: string;
    before: string;
  }

  export interface Overview {
    kind: string;
    data: Data;
  }
