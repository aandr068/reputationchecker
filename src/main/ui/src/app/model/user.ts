export class User{
  constructor(private _user_name?: String,
              private _access_token?: String,
              private _id?: String) { }

  get user_name(): String {
    return this._user_name;
  }

  set user_name(value: String) {
    this._user_name = value;
  }

  get access_token(): String {
    return this._access_token;
  }

  set access_token(value: String) {
    this._access_token = value;
  }

  get id(): String {
    return this._id;
  }

  set id(value: String) {
    this._id = value;
  }
}
