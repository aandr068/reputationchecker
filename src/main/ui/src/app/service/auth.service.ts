import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../model/user";

@Injectable()
export class AuthService {
  private user:User;

  private shouldAuth:Boolean = true;

  constructor(private http: HttpClient) { }

  public authenticate(openAuthModal, openErrorModal) {
    return this.http.get(`${location.origin}/auth`).subscribe((user:User) =>{
        this.user = user;
      },
      error => {
        error.status === 401 ? openAuthModal(error) : openErrorModal(error)});
  }


  public getUser(){
    return this.user;
  }

  public isAuth(){
    if(!this.shouldAuth){
      return true;
    }
    return this.user!=null;
  }

}
