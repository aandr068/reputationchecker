import { Injectable } from '@angular/core';
import {AuthService} from "./auth.service";

@Injectable()
export abstract class RedditHttpService {

  protected redditUrl:string = "https://www.reddit.com";

  protected constructor(protected authService: AuthService) { }

  protected getGenerics(){
    return {
      headers:{
        Authorization:this.getAuth(),
        "Content-Type":"application/x-www-form-urlencoded",
      }
    }
  }


  protected getAuth(){
     let s = `Bearer ${this.authService.getUser().access_token}`;
    return s;
  }



}
