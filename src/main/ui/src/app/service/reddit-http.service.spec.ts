import { TestBed, inject } from '@angular/core/testing';

import { RedditHttpService } from './reddit-http.service';

describe('RedditHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RedditHttpService]
    });
  });

  it('should be created', inject([RedditHttpService], (service: RedditHttpService) => {
    expect(service).toBeTruthy();
  }));
});
