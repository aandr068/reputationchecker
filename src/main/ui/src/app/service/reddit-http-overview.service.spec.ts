import { TestBed, inject } from '@angular/core/testing';

import { RedditHttpOverviewService } from './reddit-http-overview.service';

describe('RedditHttpOverviewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RedditHttpOverviewService]
    });
  });

  it('should be created', inject([RedditHttpOverviewService], (service: RedditHttpOverviewService) => {
    expect(service).toBeTruthy();
  }));
});
