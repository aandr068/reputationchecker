import { Injectable } from '@angular/core';
import {RedditHttpService} from "./reddit-http.service";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "./auth.service";

@Injectable()
export class RedditHttpOverviewService extends RedditHttpService{

  private overviewPath = (userName, after)=> `/user/${userName}/overview/.json?count=25&after=${after}`;

  constructor(protected http: HttpClient, protected authService: AuthService) {
    super(authService);
  }

  public getOverview(userName, after){
    var url = this.redditUrl+this.overviewPath(userName, after);
    console.log(url);
    return this.http.get(url);
  }

}
