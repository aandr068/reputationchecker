import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { RedditorSearchComponent } from './redditor-search/redditor-search.component';
import { AppComponent } from './app.component';
import { OverviewComponent } from './overview/overview.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RoutesConfig } from "./modules/app-routes.module";
import { AppBootstrapModule } from "./modules/app-bootstrap.module";
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AuthModalComponent } from './auth-modal/auth-modal.component';
import { ErrorModalComponent } from './error-modal/error-modal.component';
import { AboutComponent } from './about/about.component';
import { AuthService} from "./service/auth.service";
import { RedditHttpOverviewService } from "./service/reddit-http-overview.service";
import {DatePipe} from "@angular/common";

@NgModule({
  declarations: [
    AppComponent,
    RedditorSearchComponent,
    OverviewComponent,
    PageNotFoundComponent,
    NavBarComponent,
    AuthModalComponent,
    ErrorModalComponent,
    AboutComponent
  ],
  imports: [
    RoutesConfig,
    BrowserModule,
    FormsModule,
    AppBootstrapModule,
    HttpClientModule,
  ],
  entryComponents: [
    AuthModalComponent,
    ErrorModalComponent
  ],
  providers: [
    AuthService,
    RedditHttpOverviewService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
 export class AppModule { }
