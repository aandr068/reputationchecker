import {Component, Injectable, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {BsModalService} from 'ngx-bootstrap';
import {AuthModalComponent} from "./auth-modal/auth-modal.component";
import {ErrorModalComponent} from "./error-modal/error-modal.component";
import {NavigationEnd, Router} from "@angular/router";
import {AuthService} from "./service/auth.service";

@Injectable()
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Reputation Tester';
  user = null;
  constructor(private http: HttpClient,
              private modalService: BsModalService,
              private router: Router,
              private authService: AuthService) {}

  ngOnInit() {
    this.router.events.subscribe(value => {
      if(value instanceof NavigationEnd && value.url === "/"){
          this.authenticate();
      }
    });
  }

  private authenticate() {
    this.authService.authenticate(
      (error)=>this.openAuthModal(error),
      (error)=>this.openErrorModal(error)
    );
  }

  private openAuthModal(error) {
    let bsModalRef = this.modalService.show(AuthModalComponent);
    bsModalRef.content.authUrl = error.error.auth_url;
    bsModalRef.content.onClose.subscribe(result => {
      this.authenticate();
    });
  }

  private openErrorModal(error) {
    console.log(error);
    this.modalService.show(ErrorModalComponent);
  }
}
