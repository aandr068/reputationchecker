import { Component, OnInit } from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  activeTab:string;

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe(value => {
      if(value instanceof NavigationEnd){
        this.activeTab = value.url;
      }
    });
  }

  public isActive(tab:string){
    if(this.activeTab === tab){
      return "nav-item-active";
    }
    return "";
  }

}
