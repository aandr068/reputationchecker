import { Component, OnInit } from '@angular/core';
import {User} from "../model/user";
import {AuthService} from "../service/auth.service";
import {Router} from '@angular/router';

@Component({
  selector: 'app-redditor-search',
  templateUrl: './redditor-search.component.html',
  styleUrls: ['./redditor-search.component.css']
})
export class RedditorSearchComponent implements OnInit {
  user: User;

  constructor(private authService:AuthService,
              private router:Router) { }

  onSubmit(user:User) {
    console.log(user);
    this.router.navigateByUrl(`${user.user_name}/overview`)
  }

  ngOnInit() {
    this.user = new User();
  }


  isAuth(){
    return this.authService.isAuth();
  }

}
