import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedditorSearchComponent } from './redditor-search.component';

describe('RedditorSearchComponent', () => {
  let component: RedditorSearchComponent;
  let fixture: ComponentFixture<RedditorSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedditorSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedditorSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
