import { RedditorSearchComponent } from '../redditor-search/redditor-search.component';
import { OverviewComponent } from '../overview/overview.component';
import {PageNotFoundComponent} from "../page-not-found/page-not-found.component";
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from "../about/about.component";


const appRoutes: Routes = [
  { path: '',         component: RedditorSearchComponent },
  { path: ':username/overview', component: OverviewComponent },
  { path: 'about',    component: AboutComponent },
  { path: '**',       component: PageNotFoundComponent }
];


const routesConfig = RouterModule.forRoot(appRoutes, { enableTracing: false, useHash: true });

export {routesConfig as RoutesConfig}
