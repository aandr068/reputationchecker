import { Component, OnInit, ViewChild } from '@angular/core';
import {RedditHttpOverviewService} from "../service/reddit-http-overview.service";
import {ActivatedRoute} from "@angular/router";
import {Child, Overview} from "../model/overview";
import * as _ from "lodash";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  numberOfComments:number = 3000;

  private userName:String;
  list: Overview[] = [];
  startCalculating: boolean = false;
  constructor(private redditHttpOverview: RedditHttpOverviewService,
              private route: ActivatedRoute,
              private pipe: DatePipe) {}


  ngOnInit() {
    this.getParamValues();
  }


  getLast10Comments(){
    return _.flatMap(this.list, overview =>
      _.map(overview.data.children, child=>
      {return {subreddit: child.data.subreddit, comment: child.data.body, url:child.data.permalink}})).slice(0,11);
  }

  getTop10Subs(){
    if(this.startCalculating){
      let mapCleaner = {};
      let subreddits = _.flatMap(this.list, overview =>_.map(overview.data.children, child=>
      {return {"subreddit": child.data.subreddit, "dateCreated": child.data.created}}));

      let sortByAll = _.sortBy(subreddits, ['subreddit', 'dateCreated']);
      sortByAll.forEach(subreddits=>{
        mapCleaner[subreddits.subreddit] = subreddits.dateCreated;
      });
      //let arr = _.chain(subreddits).countBy(x=>x.subreddit).toPairs().sortBy().reverse().map(x=>x[0]).value();
      let displayArray = [];
      Object.keys(mapCleaner).forEach(keys=>{
        displayArray.push({subreddit:keys,lastPosted:mapCleaner[keys]})
      });
      return _.sortBy(displayArray, "lastPosted").reverse().slice(0,11);
    }
  }


  formatDate(input:number){
    return this.pipe.transform(new Date(input*1000), "'M/d/yy, h:mm a ZZZZ")
  }

  getOverview(userName, after, count?){
      this.redditHttpOverview.getOverview(userName, after).subscribe((overview: Overview) =>{
          this.list.push(overview);
          if(this.list.length<count){
            var nextPull = overview.data.after;
            this.getOverview(userName, nextPull, count)
          }else{
            this.startCalculating = true;
          }
        },
        error => console.log(error));
  }

  getParamValues() {
    this.route.params.subscribe(params => {
      this.userName = params['username'];
      this.getOverview(this.userName,"", this.numberOfComments/25);
    });
  }
}
